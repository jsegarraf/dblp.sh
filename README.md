# dblp.sh

## Description

*dblp.sh* is a command line script equivalent to [dblp](https://github.com/grundprinzip/dblp), which seems that no longer works.
The idea is that you don't have to manually maintain the required bibtex entries in your own file, but use DBLP keys and automatically generate a bib file with the corresponding entries from [DBLP](https://dblp.uni-trier.de/) instead.


## Installation

To install the [dblp script](dblp) just copy it to any directory in your $PATH.
It requires the `wget` network downloader to be installed.


## Synopsis

The first step is to build your latex document to generate the aux file.
Next, call ```dblp my_tex_file[.tex|.aux]``` and this will scan for the citation commands in the aux file.
The DBLP keys found will be used to query DBLP, and entries will be saved to the dblp.bib file.
To use this file in your LaTeX document just use `\bibliography{my_own_file,..., dblp}`.


